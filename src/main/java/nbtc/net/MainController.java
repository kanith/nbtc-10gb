package nbtc.net;

import redis.clients.jedis.Jedis;

import java.util.Set;

public class MainController {
    public static void main(String[] args) {

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.out.println("Shutdown hook ran!");
            }
        });
        try {
//            System.out.println("Hello world !!");
            Jedis jedis = new Jedis("localhost");
            jedis.set("foo", "bar ");
            jedis.set("foo2", "bar 222");
            jedis.set("foo3", "bar 333");
            jedis.set("foo4", "bar 444");
//            String value = jedis.get("foo");
            Set<String> names = jedis.keys("*");
            java.util.Iterator<String> it = names.iterator();
            while(it.hasNext()) {
                String s = it.next();
                System.out.println(s + " : " + jedis.get(s));
            }
//            System.out.println("value = " + value);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("InterruptedException hook ran!");

        }
    }
}

